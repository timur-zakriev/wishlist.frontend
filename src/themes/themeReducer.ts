/**
 * описание типа темы
 */
import { Theme } from './styledThemes'

/**
 * 
 * @param state обьект темы
 * @param action событие
 */
const themeReducer = (theme: Theme, action: any) => {
  const main = theme.main;
  switch (action.type) {
    case 'switch':
      return { 
        ...theme,
        main: theme.secondary,
        secondary: main,
      };

    default:
      throw new Error();
  }
}

export default themeReducer;