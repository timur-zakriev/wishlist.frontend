/** библиотеки */
import React from 'react'
import styled from '../../themes/styledThemes'

/** константы */
import { CheckboxStyleTypes } from './constants'

/**
 * Props компонента Input
 */
export interface CheckboxProps extends React.HTMLProps<HTMLInputElement> {
  /** Название класса */
  className?: string;
  /** React Children */
  placeholder?: string;
  /** Вариант оформления кнопки. По умолчанию 'main'
   * @default ButtonStyleTypes.MAIN
   * */
  styleType?: CheckboxStyleTypes
}

/**
 * Компонент кнопки без стилей
 * @param placehoder
 * @param className
 */
const UnstyledCheckbox: React.FC<CheckboxProps>= ({ placeholder, className }: CheckboxProps) => {
  return (
    <label className={className}>
      <input type="checkbox" className="checkbox"/>
      <span className="checkbox-text">{placeholder}</span>
    </label>
  )
}

/**
 * Styled-компонент. Стилизованная версия чекбокса
 */
const CheckboxStyled = styled(UnstyledCheckbox)`
  ${({ theme, styleType = CheckboxStyleTypes.MAIN }) => `
    cursor: pointer;

    .checkbox-text {
      position: relative;
      margin-left: 25px;
      user-select: none;
      color: ${theme.main.checkBox[styleType].color}
    }
    .checkbox-text::before {
      content: '';
      width: 18px;
      height: 18px;
      position: absolute;
      left: -25px;
      border: 1px solid ${theme.main.checkBox[styleType].backgroundColor};
      border-radius: 50%;
    }
    input {
      display: none;

      &:checked ~ .checkbox-text::before {
        background-color: ${theme.main.checkBox[styleType].backgroundColor};
      }
  
      &:disabled ~ .checkbox-text::before {
      }
    }
  `}
`;

/**
 * Компонент чекбокс
 * @param placeholder
 * @param className
 */
const Checkbox: React.FC<CheckboxProps> = ({ placeholder, className, styleType }: CheckboxProps) => (
  <CheckboxStyled className={className} placeholder={placeholder} styleType={styleType}/>
);

export default Checkbox;
