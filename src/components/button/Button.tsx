/** библиотеки */
import React from 'react'
import styled from '../../themes/styledThemes'

/** константы */
import { ButtonStyleTypes } from './constants'
import { LegacyRefHelper } from '../../utils/typescriptHelper'


/**
 * Props компонента Button
 */
export interface ButtonProps extends React.HTMLProps<HTMLButtonElement> {
  /** Children кнопки */
  children?: string;
  /** Имя класса, необходимо для styled-components */
  className?: string;
  /** Вариант оформления кнопки. По умолчанию 'main'
   * @default ButtonStyleTypes.MAIN
   * */
  styleType?: ButtonStyleTypes;
}


/**
 * Компонент кнопки без стилей
 * @param children
 * @param className
 */
const UnstyledButton: React.FC<ButtonProps> = ({ children, className, styleType, type,  ...props }: ButtonProps) => {
  const buttonType = type as 'reset' | 'button' | 'submit' | undefined;
  return (
    <button {...props} className={className} type={buttonType}>
      <span className="button__content">{children}</span>
    </button>
  )
}

const StyledButton = styled(UnstyledButton)`
  ${({ styleType = ButtonStyleTypes.MAIN, theme }) => `
  box-sizing: border-box;
  display: inline-flex;
  justify-content: center;
  align-items: center;
  min-width: ${theme.main.buttons[styleType].minWidth};
  border: ${theme.main.buttons[styleType].border};
  border-radius: ${theme.main.buttons[styleType].borderRadius};
  background-color: ${theme.main.buttons[styleType].backgroundColor};
  color: ${theme.main.buttons[styleType].color};
  padding: ${theme.main.buttons[styleType].padding};
  outline: none;
  transition: ${theme.main.buttons[styleType].transition};
  position: relative;
  
  &:hover {
    background-color: ${theme.main.buttons[styleType].hover.backgroundColor};
    border-color: ${theme.main.buttons[styleType].hover.borderColor};
    cursor: pointer;
    
    .button__content {
      color: ${theme.main.buttons[styleType].hover.color};
    }
  }

  &:active {
    background-color: ${theme.main.buttons[styleType].active.backgroundColor};
    border-color: ${theme.main.buttons[styleType].active.borderColor};
    
    .button__content {
      color: ${theme.main.buttons[styleType].active.color};
    }
  }
  
  &:disabled {
    background-color: ${theme.main.buttons[styleType].disabled.backgroundColor};
    border-color: ${theme.main.buttons[styleType].disabled.borderColor};
    opacity: ${theme.main.buttons[styleType].disabled.opacity};
    cursor: ${theme.main.buttons[styleType].disabled.cursor};
    
    .button__content {
      color: ${theme.main.buttons[styleType].disabled.color};
    }
  }
  
  .button__content {
    color: ${theme.main.buttons[styleType].color};
    font-family: ${theme.main.typography.text2.fontFamily};
    line-height: ${theme.main.typography.text2.lineHeight};
    font-weight: ${theme.main.typography.text2.fontWeight};
  }
  `}
`;


/**
 * Компонент кнопки
 * @param children
 * @param className
 * @param styleType
 */
const Button: React.FC<ButtonProps> = ({ children, className, styleType, ref, ...props }: ButtonProps) => (
  <StyledButton className={className} ref={ref as LegacyRefHelper<HTMLButtonElement>} styleType={styleType} {...props}>{children}</StyledButton>
);

export default Button;
