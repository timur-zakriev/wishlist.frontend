/** библиотеки */
import React, { useReducer } from 'react'

/** компоненты */
import Button from './button/Button'
import Checkbox from './checkbox/Checkbox'

/** стили */
import './Demo.css'
import { ThemeProvider } from 'emotion-theming'
import defaultTheme from '../themes/default'
import ThemeReducer from '../themes/themeReducer'

/**
 * демо страница с компонентами
 */
export const Demo = () => {
  const [theme, dispatch] = useReducer(ThemeReducer, defaultTheme)

  return (
    <ThemeProvider theme={theme}>
      <div className="demo" style={{background: theme.main.block.background }}>
        <Button onClick={() => dispatch({type: 'switch'})}>
          Изменить тему
        </Button>
        <Checkbox placeholder="Переключатель"/>
      </div>
    </ThemeProvider>
  )
}
